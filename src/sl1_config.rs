
use serde::Deserialize;
use serde_ini;
use std::io;

#[derive(Debug,Deserialize)]
pub struct Sl1Config {
	#[serde(rename="jobDir")]
	pub job_dir: String,
	#[serde(rename="layerHeight")]
	pub layer_height: f64,
	#[serde(rename="numSlow")]
	pub num_slow: u32,
	#[serde(rename="numFast")]
	pub num_fast: u32,
	#[serde(rename="expTime")]
	pub exp_time: f64,
	#[serde(rename="expTimeFirst")]
	pub exp_time_first: f64,
}
impl Sl1Config {
	pub fn parse_from_ini<R: io::Read>(reader: R) -> serde_ini::de::Result<Self> {
		serde_ini::from_read::<R, Self>(reader)
	}
}
