
use serde::Serialize;
use serde_json;
use std::io;

use crate::sl1_config::Sl1Config;

#[derive(Debug,Serialize)]
#[allow(non_snake_case)] // TODO: rename these instead
pub struct MonopriceConfig {
	#[serde(rename="BurnInApproachMicronsPerSec")]
	pub BurnInApproachMicronsPerSec: u32,
	#[serde(rename="BurnInApproachRPM")]
	pub BurnInApproachRPM: u32,
	#[serde(rename="BurnInApproachRotJerk")]
	pub BurnInApproachRotJerk: u32,
	#[serde(rename="BurnInApproachWaitMS")]
	pub BurnInApproachWaitMS: u32,
	#[serde(rename="BurnInApproachZJerk")]
	pub BurnInApproachZJerk: u32,
	#[serde(rename="BurnInExposureSec")]
	pub BurnInExposureSec: f64,
	#[serde(rename="BurnInLayers")]
	pub BurnInLayers: u32,
	#[serde(rename="BurnInPressMicrons")]
	pub BurnInPressMicrons: u32,
	#[serde(rename="BurnInPressMicronsPerSec")]
	pub BurnInPressMicronsPerSec: u32,
	#[serde(rename="BurnInPressWaitMS")]
	pub BurnInPressWaitMS: u32,
	#[serde(rename="BurnInRotationMilliDegrees")]
	pub BurnInRotationMilliDegrees: u32,
	#[serde(rename="BurnInSeparationMicronsPerSec")]
	pub BurnInSeparationMicronsPerSec: u32,
	#[serde(rename="BurnInSeparationRPM")]
	pub BurnInSeparationRPM: u32,
	#[serde(rename="BurnInSeparationRotJerk")]
	pub BurnInSeparationRotJerk: u32,
	#[serde(rename="BurnInSeparationZJerk")]
	pub BurnInSeparationZJerk: u32,
	#[serde(rename="BurnInUnPressMicronsPerSec")]
	pub BurnInUnPressMicronsPerSec: u32,
	#[serde(rename="BurnInZLiftMicrons")]
	pub BurnInZLiftMicrons: u32,
	#[serde(rename="FirstApproachMicronsPerSec")]
	pub FirstApproachMicronsPerSec: u32,
	#[serde(rename="FirstApproachRPM")]
	pub FirstApproachRPM: u32,
	#[serde(rename="FirstApproachRotJerk")]
	pub FirstApproachRotJerk: u32,
	#[serde(rename="FirstApproachWaitMS")]
	pub FirstApproachWaitMS: u32,
	#[serde(rename="FirstApproachZJerk")]
	pub FirstApproachZJerk: u32,
	#[serde(rename="FirstExposureSec")]
	pub FirstExposureSec: f64,
	#[serde(rename="FirstPressMicrons")]
	pub FirstPressMicrons: u32,
	#[serde(rename="FirstPressMicronsPerSec")]
	pub FirstPressMicronsPerSec: u32,
	#[serde(rename="FirstPressWaitMS")]
	pub FirstPressWaitMS: u32,
	#[serde(rename="FirstRotationMilliDegrees")]
	pub FirstRotationMilliDegrees: u32,
	#[serde(rename="FirstSeparationMicronsPerSec")]
	pub FirstSeparationMicronsPerSec: u32,
	#[serde(rename="FirstSeparationRPM")]
	pub FirstSeparationRPM: u32,
	#[serde(rename="FirstSeparationRotJerk")]
	pub FirstSeparationRotJerk: u32,
	#[serde(rename="FirstSeparationZJerk")]
	pub FirstSeparationZJerk: u32,
	#[serde(rename="FirstUnPressMicronsPerSec")]
	pub FirstUnPressMicronsPerSec: u32,
	#[serde(rename="FirstZLiftMicrons")]
	pub FirstZLiftMicrons: u32,
	#[serde(rename="JobName")]
	pub JobName: String,
	#[serde(rename="LayerThicknessMicrons")]
	pub LayerThicknessMicrons: u32,
	#[serde(rename="ModelApproachMicronsPerSec")]
	pub ModelApproachMicronsPerSec: u32,
	#[serde(rename="ModelApproachRPM")]
	pub ModelApproachRPM: u32,
	#[serde(rename="ModelApproachRotJerk")]
	pub ModelApproachRotJerk: u32,
	#[serde(rename="ModelApproachWaitMS")]
	pub ModelApproachWaitMS: u32,
	#[serde(rename="ModelApproachZJerk")]
	pub ModelApproachZJerk: u32,
	#[serde(rename="ModelExposureSec")]
	pub ModelExposureSec: f64,
	#[serde(rename="ModelPressMicrons")]
	pub ModelPressMicrons: u32,
	#[serde(rename="ModelPressMicronsPerSec")]
	pub ModelPressMicronsPerSec: u32,
	#[serde(rename="ModelPressWaitMS")]
	pub ModelPressWaitMS: u32,
	#[serde(rename="ModelRotationMilliDegrees")]
	pub ModelRotationMilliDegrees: u32,
	#[serde(rename="ModelSeparationMicronsPerSec")]
	pub ModelSeparationMicronsPerSec: u32,
	#[serde(rename="ModelSeparationRPM")]
	pub ModelSeparationRPM: u32,
	#[serde(rename="ModelSeparationRotJerk")]
	pub ModelSeparationRotJerk: u32,
	#[serde(rename="ModelSeparationZJerk")]
	pub ModelSeparationZJerk: u32,
	#[serde(rename="ModelUnPressMicronsPerSec")]
	pub ModelUnPressMicronsPerSec: u32,
	#[serde(rename="ModelZLiftMicrons")]
	pub ModelZLiftMicrons: u32,
	#[serde(rename="RotateHomeOnApproach")]
	pub RotateHomeOnApproach: u32,
}
impl Default for MonopriceConfig {
	fn default() -> Self {
		// Stolen from pig2.tar.gz
		MonopriceConfig {
			BurnInApproachMicronsPerSec: 500,
			BurnInApproachRPM: 4,
			BurnInApproachRotJerk: 100000,
			BurnInApproachWaitMS: 5000,
			BurnInApproachZJerk: 99999,
			BurnInExposureSec: 4.000,
			BurnInLayers: 4,
			BurnInPressMicrons: 0,
			BurnInPressMicronsPerSec: 1500,
			BurnInPressWaitMS: 0,
			BurnInRotationMilliDegrees: 60000,
			BurnInSeparationMicronsPerSec: 1500,
			BurnInSeparationRPM: 4,
			BurnInSeparationRotJerk: 100000,
			BurnInSeparationZJerk: 99999,
			BurnInUnPressMicronsPerSec: 1500,
			BurnInZLiftMicrons: 750,
			FirstApproachMicronsPerSec: 500,
			FirstApproachRPM: 4,
			FirstApproachRotJerk: 100000,
			FirstApproachWaitMS: 5000,
			FirstApproachZJerk: 99999,
			FirstExposureSec: 8.000,
			FirstPressMicrons: 0,
			FirstPressMicronsPerSec: 1500,
			FirstPressWaitMS: 0,
			FirstRotationMilliDegrees: 60000,
			FirstSeparationMicronsPerSec: 1500,
			FirstSeparationRPM: 4,
			FirstSeparationRotJerk: 100000,
			FirstSeparationZJerk: 99999,
			FirstUnPressMicronsPerSec: 1500,
			FirstZLiftMicrons: 750,
			JobName: "Piggy_Bank".into(),
			LayerThicknessMicrons: 50,
			ModelApproachMicronsPerSec: 1500,
			ModelApproachRPM: 12,
			ModelApproachRotJerk: 100000,
			ModelApproachWaitMS: 1500,
			ModelApproachZJerk: 99999,
			ModelExposureSec: 2.200,
			ModelPressMicrons: 0,
			ModelPressMicronsPerSec: 1500,
			ModelPressWaitMS: 0,
			ModelRotationMilliDegrees: 60000,
			ModelSeparationMicronsPerSec: 1500,
			ModelSeparationRPM: 8,
			ModelSeparationRotJerk: 100000,
			ModelSeparationZJerk: 99999,
			ModelUnPressMicronsPerSec: 1500,
			ModelZLiftMicrons: 750,
			RotateHomeOnApproach: 0,
		}
	}
}
impl MonopriceConfig {
	pub fn from_sl1_config(other: &Sl1Config) -> Self {
		MonopriceConfig {
			JobName: other.job_dir.clone(),
			LayerThicknessMicrons: (other.layer_height * 1000.0) as u32,
			FirstExposureSec: other.exp_time_first,
			ModelExposureSec: other.exp_time,
			..Default::default()
		}
	}
	
	pub fn write_to_json<W: io::Write>(self, writer: W) -> serde_json::Result<()> {
		let top: MonopriceConfigTop = self.into();
		serde_json::to_writer_pretty(writer, &top)
	}
}
impl Into<MonopriceConfigTop> for MonopriceConfig {
	fn into(self) -> MonopriceConfigTop {
		MonopriceConfigTop {
			settings: self
		}
	}
}

#[derive(Debug,Serialize)]
pub struct MonopriceConfigTop {
	#[serde(rename="Settings")]
	pub settings: MonopriceConfig,
}
