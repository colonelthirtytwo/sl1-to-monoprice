
extern crate libflate;
extern crate png;
extern crate serde;
extern crate serde_ini;
extern crate serde_json;
extern crate structopt;
extern crate tar;
extern crate zip;

mod monoprice_config;
mod sl1_config;

use png::HasParameters;
use std::fs;
use std::io::{self, Read as _, Write as _};
use std::path::PathBuf;
use std::str::FromStr;
use structopt::StructOpt;

use sl1_config::Sl1Config;
use monoprice_config::MonopriceConfig;

#[derive(Debug,StructOpt)]
struct Args {
	/// Input path to a .sl1 file
	in_file: PathBuf,
	/// Output path to a .tar.gz file
	out_file: PathBuf,
	
	/// Skip rotating the images. Dunno if this works.
	#[structopt(long="skip-rotate")]
	skip_rotate: bool,
}

fn main() {
	if let Err(err) = real_main() {
		eprintln!("{}", err);
		::std::process::exit(1);
	}
}

fn real_main() -> Result<(), String> {
	let args = Args::from_args();
	
	let in_file = io::BufReader::new(
		fs::File::open(args.in_file)
			.map_err(|err| format!("Could not open input file: {}", err))?
	);
	
	let mut in_zip = zip::ZipArchive::new(in_file)
		.map_err(|err| format!("Could not read zip file: {}", err))?;
	
	let out_file = io::BufWriter::new(
		fs::File::create(args.out_file)
			.map_err(|err| format!("Could not open output file: {}", err))?
	);
	
	let out_compressed = libflate::gzip::Encoder::new(out_file)
		.map_err(|err| format!("Could not write output file: {}", err))?;
	
	let mut out_tar = tar::Builder::new(out_compressed);
	
	let in_config_file = in_zip.by_name("config.ini")
		.map_err(|err| format!("Could not read config.ini in input file: {}", err))?;
	let in_config_mtime = in_config_file.last_modified().to_time().to_timespec().sec as u64;
	
	let in_config = Sl1Config::parse_from_ini(in_config_file)
		.map_err(|err| format!("Could not parse config.ini in input file: {}", err))?;
	
	let num_layers = in_config.num_slow + in_config.num_fast;
	
	write_to_tar(&mut out_tar, "printsettings", in_config_mtime, |mut w|
		Ok(MonopriceConfig::from_sl1_config(&in_config).write_to_json(&mut w).expect("Writing tar to memory failed"))
	)?;
	
	for i in 0..num_layers {
		print!("\rConverting layer {:5} / {:5}", i+1, num_layers);
		let _ = io::stdout().flush();
		
		let in_png_path = format!("{}{:05}.png", in_config.job_dir, i);
		let mut in_png_file = in_zip.by_name(&in_png_path)
			.map_err(|err| format!("Could not read slice {}: {}", i, err))?;
		let in_png_mtime = in_png_file.last_modified().to_time().to_timespec().sec as u64;
		
		if args.skip_rotate {
			write_to_tar(&mut out_tar, &format!("slice_{}.png", i+1), in_png_mtime, |mut w| {
				in_png_file.read_to_end(&mut w)
					.map_err(|err| format!("Could not read slice {}: {}", i, err))?;
				Ok(())
			})?;
		} else {
			let mut in_decoder = png::Decoder::new(in_png_file);
			in_decoder.set(
				png::Transformations::EXPAND |
				png::Transformations::STRIP_ALPHA
			);
			let (in_png_info, mut in_png_reader) = in_decoder.read_info()
				.map_err(|err| format!("Could not read slice {}'s png info: {}", i, err))?;
			
			if in_png_info.color_type != png::ColorType::Grayscale {
				return Err(format!("Could not read slice {}'s png data: color type is {:?}, which is unsupported", i, in_png_info.color_type));
			}
			
			if in_png_info.bit_depth != png::BitDepth::Eight {
				return Err(format!("Could not read slice {}'s png data: bit depth is {:?}, which is unsupported", i, in_png_info.bit_depth));
			}
			
			let mut in_png_data = vec![0; in_png_info.buffer_size()];
			in_png_reader.next_frame(&mut in_png_data)
				.map_err(|err| format!("Could not read slice {}'s png data: {}", i, err))?;
			
			let mut out_png_data = vec![0; in_png_info.width as usize * in_png_info.height as usize];
			rotate_png(&in_png_info, &in_png_data, &mut out_png_data);
			
			write_to_tar(&mut out_tar, &format!("slice_{}.png", i+1), in_png_mtime, |w| {
				let mut out_encoder = png::Encoder::new(w, in_png_info.height, in_png_info.width);
				out_encoder.set(png::ColorType::Grayscale);
				out_encoder.set(png::BitDepth::Eight);
				let mut out_writer = out_encoder.write_header().expect("Could not write png to memory");
				out_writer.write_image_data(&out_png_data).expect("Could not write png to memory");
				Ok(())
			})?;
		}
	}
	println!("");
	out_tar
		.into_inner()
		.map_err(|err| format!("Could not finish writing output file: {}", err))?
		.finish()
		.into_result()
		.map_err(|err| format!("Could not finish writing output file: {}", err))?
	;
	Ok(())
}

/// Convenience function for writing a file to a tar file.
fn write_to_tar<W: io::Write, F: FnOnce(&mut Vec<u8>) -> Result<(), String>>(
	tar: &mut tar::Builder<W>,
	filename: &str,
	mtime: u64,
	func: F
) -> Result<(), String> {
	let mut buf = vec![];
	let path = PathBuf::from_str(filename).unwrap();
	func(&mut buf)?;
	
	let mut header = tar::Header::new_old();
	header.set_size(buf.len() as u64);
	header.set_path(&path).expect("Could not encode path; should never happen");
	header.set_mode(0o644);
	header.set_mtime(mtime);
	header.set_cksum(); // Must be last!
	tar.append(&header, io::Cursor::new(&buf))
		.map_err(|err| format!("Could not write tar entry: {}", err))
}

fn rotate_png(info: &png::OutputInfo, in_data: &[u8], out_data: &mut [u8]) {
	for x in 0..info.width as usize {
		for y in 0..info.height as usize {
			let in_value = in_data[y * info.line_size as usize + x];
			out_data[x * info.height as usize + y] = in_value;
		}
	}
}
